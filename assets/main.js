(function(){
    
  /* render all list items upon window load */
    window.onload= function(){
          getAllTodoItems();
    }

    
/* make a get call using the passed url */
function makeGetCall(url){
  return axios.get(url)
}

/* this function renders all todo items */
function getAllTodoItems(){
  var promise = makeGetCall('https://timesheet-1172.appspot.com/3852180f/notes');
    promise.then(function (response) {
      var itemsList = response.data;
      if (itemsList.length>0){
        for(counter =0; counter<itemsList.length; counter++){
          // render each item
          renderedItem = renderListItem(itemsList[counter]);
            // append the returned item
            $(".todos-list").append(renderedItem);
        }
      }
    })
    .catch(function (error) {
      console.log(error);
    });
}
    
    
/* slide toggle the add a todo form */
$("#add-item").on("click",function(){
    // side toggle the form
$("#add-item-form-box").slideToggle(500);
    // hide the validation text
$(".validation-text").removeClass("show");
});
   
    
/*  submit todo item form*/
$("#save-item-btn").on("click",function(event){
  // stop default form submission
event.preventDefault();
var title = $("#item-title").val();
var form = $("#add-item-form");
// validate the form
if(validateTodoForm(form, title)){
  // add the item
  addTodoItem();
  // hide the form
  $("#add-item-form-box").slideToggle(500);
  // empy form fields
  document.getElementById("add-item-form").reset();
}
});

    
/* this function adds a todo item */ 
function addTodoItem(){
    // set headers
  var headers = {
             'Content-Type': 'application/json',
         }
  // make a post request
   axios.post('https://timesheet-1172.appspot.com/3852180f/notes',{
           title: $("#item-title").val(),
           description:  $("#item-description").val(),
         }, headers).then(function(response){
           // render item dynamically
           renderedItem = renderListItem(response.data);
           // add to the top of the list
           $(".todos-list").prepend(renderedItem);
         }).catch(function(error){
                console.log(error);
  });
}

/* submit edit todo item form */
$("#edit-item-btn").on("click", function(event){
  // stop default form submission 
event.preventDefault();
var title = $("#item-title-edit").val();
var form = $("#add-item-form-edit");
id = $("#item-title-edit").attr("name").split("_")[1];
// validate the form
if(validateTodoForm(form , title)){
  // edit todo item
  editTodoItem(id);
    // hide modal
$('#editModel').modal('hide');
}
});

/* this function edits a todo item */ 
function editTodoItem(id){
        // set the headers
        var headers = {
             'Content-Type': 'application/json',
         }
         // make put request
         axios.put('https://timesheet-1172.appspot.com/3852180f/notes/'+id,{
           id:id,
           title: $("#item-title-edit").val(),
           description:  $("#item-description-edit").val(),
         }, headers).then(function(response){
            item = response.data;
           // change item title dynamically
           todoItem = $("#"+item.id).find(".item-title").html(item.title);
         }).catch(function(error){
                console.log(error);
  });
}

/*  delete a todo item */
$(document).on('click', '.delete-item', function(event){
  var id = $(this).parents(".todo-item").attr("id");
  var removeable = $("#"+id);
  axios.delete('https://timesheet-1172.appspot.com/3852180f/notes/'+id)
  .then(function(response){
    // dynamically remove item
    removeable.remove();
  }).catch(function(error){
         console.log(error);
});
});

/* retrieve a todo item */
  $(document).on('click', '.todo-item-body', function(event){
   var id = $(this).parent().attr("id");
   var promise=makeGetCall('https://timesheet-1172.appspot.com/3852180f/notes/'+id);
 promise.then(function (response) {
     var item = response.data;
     $("#item-title-edit").val(item.title);
     $("#item-title-edit").attr("name","title_"+item.id)
     $("#item-description-edit").html(item.description);
     // show the popup modal
     $('#editModel').modal('show');
   })
   .catch(function (error){
     console.log(error);
   });
  });
    

/* this function renders a single todo item */
function renderListItem(item){

  return `

  <li>
  <div class='todo-item' id ='${item.id}'>
  <div class='todo-item-body'>
  <p class='item-title'>${item.title}</p>
  <div class='item-container'>
  <span class='delete-item'>
  <i class='fa fa-trash-o pull-right' aria-hidden='true'>
  </i>
  </span>
  </div>
  </div>
  </div>
  </li>
  `;}

/* this function validates the passed in form and title*/
function validateTodoForm(form, todoTitle){
  if(todoTitle ==undefined || todoTitle ==""){
    // show validation error
    form.find(".validation-text").addClass("show");
        // return a false
    return false;
  }else{
    return true;
  }
}
    
/* hide validation-text on modal close */
$('#editModel').on('hidden.bs.modal', function () {
    $(".validation-text").removeClass("show");
});
})();
